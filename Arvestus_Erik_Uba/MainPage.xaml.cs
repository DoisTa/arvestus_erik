﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Arvestus_Erik_Uba
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        string kasutajanimi = "user";
        string password = "SecretPassword";
        int count = 0;
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (count >= 3)
            {
                txtblock1.Text = "Sisselogimine ebaõnnestus. Rohkem katseid saamiseks ei ole";
            }
            else
            {

                if (kasutajanimitxtbox.Text == kasutajanimi && pwbox.Password == password)
                {
                    this.Frame.Navigate(typeof(Content));
                }
                else
                {
                    count++;
                    txtblock1.Text = "Sisselogimine ebaõnnestus.";
                }
            }
        }
    }
}
